# Challenge write-ups

This is a repository dedicated to my write-ups for penetration testing challenges.
I have only recently started writing write-ups so there are still few, hopefully the list will get longer soon!

## HackTheBox machines write-ups

Here is the list of the write-ups.

[MetaTwo](hackthebox_machines/metatwo/metatwo_writeup.md)

[Photobomb](hackthebox_machines/photobomb/photobomb_writeup.md)

[Precious](hackthebox_machines/precious/precious_writeup.md)

[Stocker](hackthebox_machines/stocker/stocker_writeup.md)

## HackTheBox challenges write-ups

[Behind the scenes](hackthebox_challenges/behind_the_scenes/behind_the_scenes_writeup.md)

[Racecar](hackthebox_challenges/racecar/racecar_writeup.md)

[Simple encryptor](hackthebox_challenges/simple_encryptor/simple_encryptor_writeup.md)

## PentesterLabs write-ups

[SQLi to shell ii](pentesterlab/sqli_to_shell_ii/from_sqli_to_shell_II.md)


## Related stuff

If you are here you may also be interested [in my notes](https://gitlab.com/potatochip29/penetration-testing-notes).