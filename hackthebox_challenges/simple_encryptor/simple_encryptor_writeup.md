# Simple Encryptor write-up
_April 2023_

**Spoilers ahead!**

An executable file named `encrypt`, and a file named `flag.enc` containing the flag are given.
The flag is ciphered and the `encrypt` executable was used to cipher it.
The goal is to decipher the contents of `flag.enc`.

```bash
file encrypt
```
```
encrypt: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=0bddc0a794eca6f6e2e9dac0b6190b62f07c4c75, for GNU/Linux 3.2.0, not stripped
```

One can open up Ghidra to disassemble the code and hopefully figure out what is going on.
Other than the `main` function, nothing seemed interesting in the file.
Thus, I went line-by-line of the disassembled code to figure out how the program worked.

The disassembled was as follows.
```c
undefined8 main(void)

{
  int iVar1;
  time_t tVar2;
  long in_FS_OFFSET;
  uint local_40;
  uint local_3c;
  long local_38;
  FILE *local_30;
  size_t local_28;
  void *local_20;
  FILE *local_18;
  long local_10;

  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  local_30 = fopen("flag","rb");
  fseek(local_30,0,2);
  local_28 = ftell(local_30);
  fseek(local_30,0,0);
  local_20 = malloc(local_28);
  fread(local_20,local_28,1,local_30);
  fclose(local_30);
  tVar2 = time((time_t *)0x0);
  local_40 = (uint)tVar2;
  srand(local_40);
  for (local_38 = 0; local_38 < (long)local_28; local_38 = local_38 + 1) {
    iVar1 = rand();
    *(byte *)((long)local_20 + local_38) = *(byte *)((long)local_20 + local_38) ^ (byte)iVar1;
    local_3c = rand();
    local_3c = local_3c & 7;
    *(byte *)((long)local_20 + local_38) =
         *(byte *)((long)local_20 + local_38) << (sbyte)local_3c |
         *(byte *)((long)local_20 + local_38) >> 8 - (sbyte)local_3c;
  }
  local_18 = fopen("flag.enc","wb");
  fwrite(&local_40,1,4,local_18);
  fwrite(local_20,1,local_28,local_18);
  fclose(local_18);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return 0;
}
```

Let's go over line by line to understand what the program is doing.
```c
// this is a line the compiler creates, no need to worry about it
local_10 = *(long *)(in_FS_OFFSET + 0x28);

local_30 = fopen("flag","rb");       // the flag file is opened
fseek(local_30,0,2);                 // moves to the end of the flag file
local_28 = ftell(local_30);          // local_28 = flag file length
fseek(local_30,0,0);                 // goes back to the start of the flag file
local_20 = malloc(local_28);         // allocates memory of the flag size
fread(local_20,local_28,1,local_30); // reads the file contents into the allocated memory
fclose(local_30);                    // closes the file
```
At the end of this first snippet, we have that:
- `local_28` is the flag length;
- `local_20` is a pointer to the flag contents.

Let's continue..

```c
tVar2 = time((time_t *)0x0);    // tVar2 = timestamp
local_40 = (uint)tVar2;         // local_40 = integer with timestamp
srand(local_40);                // the timestamp will work as a seed for random number generation
for (local_38 = 0; local_38 < (long)local_28; local_38 = local_38 + 1) { // for every flag character (byte)
iVar1 = rand();                 // iVar1 = random number
*(byte *)((long)local_20 + local_38) = *(byte *)((long)local_20 + local_38) ^ (byte)iVar1; // the character is XORed with iVar1
local_3c = rand();              // local_3c = random number
local_3c = local_3c & 7;        // local_3c = local_3c AND 00000111, meaning local_3c will be, at most, 7
*(byte *)((long)local_20 + local_38) =
     // logical OR between
     *(byte *)((long)local_20 + local_38) << (sbyte)local_3c | // shift left, at most 7 bits
     *(byte *)((long)local_20 + local_38) >> 8 - (sbyte)local_3c; // shift right, the inverse number of bits
}
```

The last line is a little bit complicated, lets use two examples to understand what is going on.
Suppose the bits of the character being ciphered are named `b0` to `b7`.

If `local_3c = 1`, in other words, if the first shift is 1 and the second 7 (8 - 1), what happens is the following.
```
b0 | b1 b2 b3 b4 b5 b6 b7 0  |
   | 0  0  0  0  0  0  0  b0 | b1 b2 b3 b4 b5 b6 b7
OR ---------------------------
   | b1 b2 b3 b4 b5 b6 b7 b0 |
```

And if the shift is 3...
```
b0 b1 b2 | b3 b4 b5 b6 b7 0  0  0  |
         | 0  0  0  0  0  b0 b1 b2 | b3 b4 b5 b6 b7
      OR ---------------------------
         | b3 b4 b5 b6 b7 b0 b1 b2 |
```

Essentially, what that line does is it shifts left `local_3c`, and appends the shifted part back to the end.

Let `s0` be the first shift, and `s1` the second shift.
In essence, the program does the following operation to each character `c`.
```
s0(c XOR iVar1) OR s1(c XOR iVar1)
```

While the "`OR` part of the algorithm" is easily reversible, we still do not know the seed for the random numbers, and so we cannot reverse the algorithm yet.

Let's examine the remaining lines of the code.
```c
local_18 = fopen("flag.enc","wb");  // A file flag.enc is opened
fwrite(&local_40,1,4,local_18);     // local_40, the seed, is written to the file!
fwrite(local_20,1,local_28,local_18); // local_20, the, now ciphered, flag contents, are written to the file
fclose(local_18);                   // the file is closed
```

This gives us the missing piece to reverse the `XOR` operation.
Now we can get the same random numbers using the seed to reverse the process.

With all of this information, one can write some code to decipher the flag.
```c
#include <stdio.h>
#include <stdlib.h>

int main() {
    FILE *flg = fopen("flag.enc", "rb");

    // open the file with the ciphered flag
    // we already know that the seed is 4 bytes long, so we can
    // ignore those 4 bytes from the length of the flag
    fseek(flg, 0, SEEK_END);
    uint sz = ftell(flg) - 4;
    rewind(flg);

    // set the seed for the random number generator
    uint seed;
    fread(&seed, 1, 4, flg);
    srand(seed);

    // reading the ciphered flag to a string
    char *str = malloc(sz);
    fread(str, 1, sz, flg);

    fclose(flg);

    // reverse the encryption
    for (int i = 0; i < sz; i++) {
        char rnd1 = rand();
        char rnd2 = rand() & 7;

        // reversing the shift and OR operations
        str[i] = (unsigned char) str[i] >> rnd2 | str[i] << (8 - rnd2);

        // reversing the XOR
        str[i] ^= rnd1;

        printf("%c", str[i]);
    }
    printf("\n");

    return 0;
}
```

### Thanks

Thanks to [PillTime](https://github.com/PillTime) for solving this challenge with me.
