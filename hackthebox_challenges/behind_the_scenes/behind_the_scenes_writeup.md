# Racecar write-up 
_April 2023_

**Spoilers ahead!**

An executable file named `behindthescenes` is given.
```bash
file behind_the_scenes 
```
```
behindthescenes: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=e60ae4c886619b869178148afd12d0a5428bfe18, for GNU/Linux 3.2.0, not stripped
```

When ran, the program asks for a password to be given as an argument.
```bash
./behind_the_scenes      
```
```
./challenge <password>                      
```
If the wrong password is given the program does not output anything.

I checked for interesting symbols using the `readelf` command, but nothing came up.
```bash
readelf -W --symbols behind_the_scenes
```

Using `ltrace` we can see that there are some illegal instructions being executed when the program is run.
```bash
ltrace ./behind_the_scenes potato
```
```
--- SIGILL (Illegal instruction) ---
--- SIGILL (Illegal instruction) ---
--- SIGILL (Illegal instruction) ---
+++ exited (status 0) +++                          
```
[The default handling of a `SIGILL` signal is to terminate the process and to dump the core, yet here we see that this signal is sent multiple times before the program termination.](https://man7.org/linux/man-pages/man7/signal.7.html)
One can already guess that this signal is being handled differently and being used multiple times.

One can use [Ghidra](https://github.com/NationalSecurityAgency/ghidra) to investigate this further.
By navigating to the main function, in the decompiler window we find the following code.
```c
void main(void)

{
  long in_FS_OFFSET;
  sigaction local_a8;
  undefined8 local_10;
  
  local_10 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  memset(&local_a8,0,0x98);
  sigemptyset(&local_a8.sa_mask);
  local_a8.__sigaction_handler.sa_handler = segill_sigaction;
  local_a8.sa_flags = 4;
  sigaction(4,&local_a8,(sigaction *)0x0);
  do {
    invalidInstructionException();
  } while( true );
}
```

Examining the code, we see that:
 - A signal handler is being created, most likely one for the `SIGILL` signal we saw using ltrace.
 - After this, an `invalidInstructionException` is thrown. 
By double-clicking it we see that the exception comes from a `UD2` instruction.

![UD2_instruction.png](UD2_instruction.png)

According to the [x86 Instruction Set Reference](https://mudongliang.github.io/x86/html/file_module_x86_id_318.html), the `UD2` instruction is known for:
- Acting as a `NOP` instruction, that is, doing nothing;
- Raising an invalid opcode exception (`SIGILL`).

At this point, one can also see that the `./challenge <password>` print is nowhere to be found on the main function, and that according to `ltrace` multiple `SIGILL` signals should be executed, not just one.

One can safely assume that Ghidra is not showing some code past the `UD2` instruction.
One can select the lines below the `UD2` instruction and press `D` for Ghidra to disassemble them, and the rest of the code can be seen. 

The reason why Ghidra does not show the main function completely is because it does not know that the default behaviour for handling the `SIGILL` signal has been changed (because there is a custom handler in the code).

After the rest of the code has been disassembled, by clicking in different lines of the assembly code one sees that Ghidra did not add all the code to the main function, since it is still having a hard time with the `UD2` instructions.

However, one can patch the `UD2` instructions to `NOP` instructions so that Ghidra can decompile everything onto the same function.
Note that it is not necessary to save these changes to the file, this is only necessary for code readability.

Once this is done, the main function should look like the following.
```c
undefined8 main(int param_1,long param_2)

{
  int iVar1;
  undefined8 uVar2;
  size_t sVar3;
  long in_FS_OFFSET;
  sigaction local_a8;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  memset(&local_a8,0,0x98);
  sigemptyset(&local_a8.sa_mask);
  local_a8.__sigaction_handler.sa_handler = segill_sigaction;
  local_a8.sa_flags = 4;
  sigaction(4,&local_a8,(sigaction *)0x0);
  if (param_1 == 2) {
    sVar3 = strlen(*(char **)(param_2 + 8));
    if (sVar3 == 0xc) {
      iVar1 = strncmp(*(char **)(param_2 + 8),"Itz",3);
      if (iVar1 == 0) {
        iVar1 = strncmp((char *)(*(long *)(param_2 + 8) + 3),"_0n",3);
        if (iVar1 == 0) {
          iVar1 = strncmp((char *)(*(long *)(param_2 + 8) + 6),"Ly_",3);
          if (iVar1 == 0) {
            iVar1 = strncmp((char *)(*(long *)(param_2 + 8) + 9),"UD2",3);
            if (iVar1 == 0) {
              printf("> HTB{%s}\n",*(undefined8 *)(param_2 + 8));
              uVar2 = 0;
            }
            else {
              uVar2 = 0;
            }
          }
          else {
            uVar2 = 0;
          }
        }
        else {
          uVar2 = 0;
        }
      }
      else {
        uVar2 = 0;
      }
    }
    else {
      uVar2 = 0;
    }
  }
  else {
    puts("./challenge <password>");
    uVar2 = 1;
  }
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return uVar2;
}
```

At this point one can see that what the program does is compare the argument string to the password string 3 bytes at a time.
If the argument string is the same as the password, the program prints out the flag (which is the password within the `HTB{}` tag).

At this point, one can execute the program and get the flag.
```bash
./behind_the_scenes Itz_0nLy_UD2
```
```
> HTB{Itz_0nLy_UD2}
```

[I encourage you to read more about how the signal handler works with this write-up.](https://agarmash.com/posts/htb-behindthescenes-writeup/)


### The lazy way to get the flag

An easier way to get the flag is using `hexeditor`.
However, it should be noted that this is a **reverse engineering challenge**, meaning that the goal should be to understand what is going on _behind the scenes_, which this method of solving the challenge does not require at all.

Using `hexeditor`, it is possible for one to see the flag bytes.
Knowing that there is a `./challenge <password>` print in the code, one can look for this on the binary file, hoping that the flag will be somewhere right after it.
```bash
hexeditor behind_the_scenes
```

Using the `Search for text string` function, one can search for the word `challenge`.
The flag is right after, as expected.
```
00002000  01 00 02 00  2E 2F 63 68   61 6C 6C 65  6E 67 65 20  ...../challenge
00002010  3C 70 61 73  73 77 6F 72   64 3E 00 49  74 7A 00 5F  <password>.Itz._
00002020  30 6E 00 4C  79 5F 00 55   44 32 00 3E  20 48 54 42  0n.Ly_.UD2.> HTB
00002030  7B 25 73 7D  0A 00 00 00   01 1B 03 3B  4C 00 00 00  {%s}.......;L...
00002040  08 00 00 00  E8 EF FF FF   80 00 00 00  78 F0 FF FF  ............x...
```

